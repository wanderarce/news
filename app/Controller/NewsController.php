<?php

App::uses('AppController', 'Controller');
App::import('Vendor',array('file'=>'autoload'));

class NewsController extends AppController {

    
    public $components = array('Paginator');
    public $helpers = array("Media.Media");
    
    public $paginate = array(
        'limit' =>10,
    );    

    public function index() {
        $this->Paginator->settings=$this->paginate;
        $this->News->recursive = 0;
        $this->set('news', $this->Paginator->paginate());
    }

    public function view($id = null) {
        if (!$this->News->exists($id)) {
            throw new NotFoundException(__('Invalid news'));
        }
        $options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
        $this->set('news', $this->News->find('first', $options));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->News->create();
            if ($this->News->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The news has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-error">The news could not be saved. Please, try again.</div>'));
            }
        }
        $users = $this->News->User->find('list', array('fields' => 'username'));
        $this->set(compact('users'));
        $categories = $this->News->Category->find('list', array('fields' => 'category'));
        $this->set(compact('categories'));
    }

    public function edit($id = null) {
        if (!$this->News->exists($id)) {
            throw new NotFoundException(__('Invalid news'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->News->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The news has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-error">The news could not be saved. Please, try again.</div>'));
            }
        } else {
            $options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
            $this->request->data = $this->News->find('first', $options);
        }
        $users = $this->News->User->find('list');
        $this->set(compact('users'));
        $categories = $this->News->Category->find('list', array('fields' => 'category'));
        $this->set(compact('categories'));
    }

    public function delete($id = null) {
        $this->News->id = $id;
        if (!$this->News->exists()) {
            throw new NotFoundException(__('Invalid news'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->News->delete()) {
            $this->Session->setFlash(__('<div class="alert alert-success">The news has been deleted.</div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-error">The news could not be deleted. Please, try again.</div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function listar() {
        $this->layout = 'mobtek';
        $news = $this->paginate();
        if ($this->request->is('requested')) {
            return $news;
        } else {
            $this->set('news', $news);
        }
    }

    public function destaque($destaque = null) {
        $this->layout = 'mobtek';
        $news = $this->News->findAllByDestaque($destaque);
        if ($this->request->is('requested')) {
            return $news;
        } else {
            $this->set('news', $news);
        }
    }

    public function visualizadas() {
        $this->layout = 'mobtek';
        $news = $this->News->find('all', array('order' => array('News.visualized desc'), 'limite' => array(10)));
        if ($this->request->is('requested')) {
            return $news;
        } else {
            $this->set('news', $news);
        }
    }

    public function visualizar($id = null) {
        $this->layout = 'mobtek';
        if (!$this->News->exists($id)) {
            throw new NotFoundException(__('Invalid news'));
        }

        $options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
        $news = $this->News->find('first', $options);
        $news['News']['visualized'] = $news['News']['visualized'] + 1;
        $this->News->save($news);
        $this->set(compact('news'));
    }

    public function search() {
        $this->layout = 'mobtek';
        //print_r($this->News);
        if ($this->News->set($this->request->data)) {
            $busca = $this->request->data['News']['Search'];
            $conditions = array('News.title Like' => '%' . $busca . '%');
            $res_busca = $this->News->find('all', array('conditions' => $conditions));
            //  var_dump($this->request->data);
            $this->set('news', $res_busca);
        }
    }

    public function graphs() {
        $this->News->recursive = 0;
        $categories = $this->News->Category->find('list', array('fields' => 'category', 'id','visualized'));
        $this->set(compact('categories'));
        $this->set('news', $this->Paginator->paginate());
    }

}
