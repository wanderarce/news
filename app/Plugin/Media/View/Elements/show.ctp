<style type="text/css">
	.imgs{width: 100% !important;
	 	  margin: 5px 0 15px 0;	 
	 	  overflow: auto;
	}
	.control-group{margin-top: 5px !important;}
	.img{margin-right: 3px;}

	.delete-media{width: 100% !important;
				background: #FF0000;
				color: #FFFFFF;
				bottom: inherit;
	}
</style>
<div class="imgs">
	<?php
		foreach ($this->data['Media'] as $key => $file) {
		?>
		<div class='pull-left'>

		<?php 
			echo $this->Html->image($file['path'].'/thumb_'.$file['name'],array('class'=>'img'));
			echo "<br>";
			echo $this->Html->link(
				'Remove',
				'javascript:void(0)',
				array('class'=>'delete-media','data-modelId'=>$file['id'])
				);
			?>
		</div>
		<?php
			}
		?>
</div>
