<?php
App::uses('AppModel', 'Model');
App::uses('AuditableConfig', 'Auditable.Lib');

class News extends AppModel {

    public $actsAs = array('Auditable.Auditable',
        'Media.Media' => array(
             'thumb_size'=>array(
            'width'=>100,
            'height'=>100
            )
        ),
        'Upload.Upload' => array(
            'archive' => array(
                'fields' => array(
                    'dir' => 'img_dir',
                ),
                'rule'=>array('mimeType'=>array('image/png','image/jpg','image/jpeg')),
                'message'=>'Invalid mime type',
                'thumbnailSizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '149x178',
                    'thumb' => '50x50',
                ),
                'thumbnailMethod' => 'php',
                //necessario informar o path para que seja salvo corretamente.
                'path' => '{ROOT}webroot{DS}img{DS}{field}{DS}',
                'deleteOnUpdate' => true
            )
        )
    ); //ARO    
    
    public $validate = array(
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'title' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Este campo é obrigatório.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'subtitle' => array(
        //	'notEmpty' => array(
        //	'rule' => array('notEmpty'),
        //'message' => 'Your custom message here',
        //'allowEmpty' => false,
        //'required' => false,
        //'last' => false, // Stop validation after this rule
        //'on' => 'create', // Limit validation to 'create' or 'update' operations
        //	),
        ),
        'body' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'fonte' => array(
        //	'notEmpty' => array(
        //'rule' => array('notEmpty'),
        //'message' => 'Your custom message here',
        //'allowEmpty' => false,
        //'required' => false,
        //'last' => false, // Stop validation after this rule
        //'on' => 'create', // Limit validation to 'create' or 'update' operations
        //	),
        ),
        'visualized' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'destaque' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'category_id' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
