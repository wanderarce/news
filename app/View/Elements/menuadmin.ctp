
<div class="col-md-2">
    <ul class="nav nav-pills nav-stacked">

        <li class="mktable"><i class="glyphicon glyphicon-share-alt"> <?php echo $this->Html->link('Publicar', array('controller' => 'news', 'action' => 'add'), array('class' => 'mb'));
?> 
            </i></li>
        <li class="mktable"><i class="glyphicon glyphicon-list"> <?php echo $this->Html->link('News', array('controller' => 'news', 'action' => 'index'), array('class' => 'mb'));
?>
            </i></li>

        <li class="mktable"><i class="glyphicon glyphicon-list"> <?php
                echo $this->Html->link('Category', array('controller' => 'categories', 'action' => 'index'), array('class' => 'mb'));
                ?></i></li>
        <li class="mktable"><i class="glyphicon glyphicon-check"> <?php
                echo $this->Html->link('Section', array('controller' => 'sections', 'action' => 'index'), array('class' => 'mb'));
                ?></i></li>
        <li class="mktable"><i class="glyphicon glyphicon-picture"> <?php
                echo $this->Html->link('Image', array('controller' => 'images', 'action' => 'index'), array('class' => 'mb'));
                ?></i></li>
        <li class="mktable"><i class="glyphicon glyphicon-upload"> <?php
                echo $this->Html->link('Banners', array('controller' => 'banners', 'action' => 'index'), array('class' => 'mb'));
                ?></i></li>
        <?php if (($this->Session->read('Auth.User.group_id'))==1): ?>
        <li class="mktable"><i class="glyphicon glyphicon-user"> <?php
                echo $this->Html->link('Users', array('controller' => 'users', 'action' => 'index'), array('class' => 'mb'));
                ?></i></li>
        <li class="mktable"><i class="glyphicon glyphicon-globe"> <?php
          echo $this->Html->link('Group', array('controller' => 'groups', 'action' => 'index'), array('class' => 'mb'));
        ?></i></li>
    <?php endif; ?>
        <li class="mktable"><i class="glyphicon glyphicon-tasks"> <?php echo $this->Html->link('Charts', array('controller' => 'news', 'action' => 'graphs'), array('class' => 'mb'));
        ?></i></a></li>
        <li class="mktable"><i class="glyphicon glyphicon-log-out"> <?php
                echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'), array('class' => 'mb'));
                ?></i></li>
    </ul>
</div>
