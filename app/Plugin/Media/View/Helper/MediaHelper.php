<?php

App::uses('Helper','View');



class MediaHelper extends Helper 
{

	public function file(){
		$fileInput = 
		'<script type="text/javascript">
		 $(document).ready(function(){
			$("#newMediaInput").on("click",function(e){
				e.preventDefault();
				$("#primaryMediaInput").clone().appendTo("#mediaInput");
			});
			$("#removeMediaInput").on("click",function(e){
				e.preventDefault();
				if($("#mediaInput .media-input").length > 1){
					$("#mediaInput .media-input").last().remove();
				}
			});
			$(".delete-media").on("click",function(){
				var modelId = $(this).data("modelid");
				var url = $("#hiddenMediaUrl").val();
				$(this).parent().hide();
				$.get(url+"/delete/"+modelId,function(data,status){

				});
			});
		});
		</script>
		<div id="mediaInput" class="control-group">
			<div id="primaryMediaInput" class="row media-input">
				<label class="">Images</label><br>
				<input type="file" id="mediaFile" class="pull-left file" name="data[Media][file][]">
				<button id="newMediaInput" class="pull-left btn btn-success" style="margin:0px 15px"><i class="glyphicon glyphicon-plus"></i></button>
				<button id="removeMediaInput" class="pull-left btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
		<input type = "hidden" value="'.$this->params->base.'/media/media" id="hiddenMediaUrl" />';

	return $fileInput;
	}

}