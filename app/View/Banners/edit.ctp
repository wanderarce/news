
<div class="actions">
    <b class=" glyphicon glyphicon-edit">
    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Banner.id')), array('confirm'=> __('Are you sure you want to delete # %s?', $this->Form->value('Banner.id')))); ?></b>
        <b class=" glyphicon glyphicon-edit">
    <?php echo $this->Html->link(__('List Banners'), array('action' => 'index')); ?></b>
</div>
<div class="row">
<?php echo $this->Form->create('Banner',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit Banner'); ?></legend>
                <div class="row">
                    <div class="col-md-6">
                    <?php echo $this->Form->input('id');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
		<?php echo $this->Form->input('title',array('class'=>'form-control'));?>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
			<?php echo $this->Form->input('banner',array('type'=>'file'));?>
                          </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->input('type', array('class' => 'form-control',
                        'options' => array(
                            '1' => 'Antigo', '2' => 'DestaqueLateral', '3' => 'DestaqueFaixa',
                            '4' => 'Superior', '5' => 'Conteudo', '6' => 'Rodape'
                            )));?>
                </div>
                </div>
       </fieldset>
       <hr>
<?php echo $this->Form->end(__('Submit')); ?>
</div>