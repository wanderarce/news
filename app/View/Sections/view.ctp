<div class="categories view">
    <h2><?php echo h($section['Section']['section']); ?></h2>
    
    <div class="row">
        <div class="[col-xs-12]">
            <ul class="event-list">
                <?php
                foreach ($section['Category'] as $category):
                    ?>
                    <li>
                        <h2 class = "title">
                            <?php echo $this->Html->link($category['category'], array('controller' => 'categories', 'action' => 'view', $category['id']));
                            ?>
                        </h2>
                    </li>
                    <?php
                endforeach;
                ?>
            </ul>
        </div>
    </div>
</div>
