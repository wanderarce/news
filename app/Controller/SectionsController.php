<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SectionsController extends AppController {

    public $components = array('Paginator');
    public $paginate = array(
        'limit' => 10,
    );
    
    public function index() {
        $this->Paginator->settings=$this->paginate;
        $this->Section->recursive = 0;
        $this->set('sections', $this->Paginator->paginate());
    }

    public function view($id = null) {
        $this->layout = 'mobtek';
        if (!$this->Section->exists($id)) {
            throw new NotFoundException(__('Invalid section'));
        }
        $options = array('conditions' => array('Section.' . $this->Section->primaryKey => $id));
        $this->set('section', $this->Section->find('first', $options));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Section->create();
            if ($this->Section->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The section has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-error">The section could not be saved. Please, try again.</div>'));
            }
        }
        $categories = $this->Section->Category->find('list');
        $this->set(compact('categories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Section->exists($id)) {
            throw new NotFoundException(__('Invalid section'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Section->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The section has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-error">The section could not be saved. Please, try again.</div>'));
            }
        } else {
            $options = array('conditions' => array('Section.' . $this->Section->primaryKey => $id));
            $this->request->data = $this->Section->find('first', $options);
        }
        $category = $this->Section->Category->find('list');
        $this->set(compact('$category'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Section->id = $id;
        if (!$this->Section->exists()) {
            throw new NotFoundException(__('Invalid section'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Section->delete()) {
            $this->Session->setFlash(__('<div class="alert alert-success">The section has been deleted.</div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-error">The section could not be deleted. Please, try again.</div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function menu() {
        $this->Section->recursive = 0;
        $this->set('sections', $this->Paginator->paginate());

        $notices = $this->Section->findAllBySection();
        $categories = $this->Section->Category->find('all');
        $this->set(compact('notices', 'categories'));
    }

    public function listar() {
        $sections = $this->Paginator->paginate();
        if ($this->request->is('requested')) {
            return $sections;
        } else {
            $this->set('sections', $sections);
        }
    }

    function contato() {
        $this->layout = 'mobtek';
        $Email = new CakeEmail();
    
        if ($this->request->is('post')) {
            $this->Section->set($this->request->data);
            if (!empty($this->request->data['Section']['company'])) {
                $Email->from(array($this->request->data['Section']['email'] => $this->request->data['Section']['company'] . ' - ' . $this->request->data['Section']['name']));
            } else {
                $Email->from(array($this->request->data['Section']['email'] => 'NewsEmTeste'));
            }
            $Email->to('contato@mobtek.com.br');
            $Email->subject('Email de contato via News');
            if ($Email->send($this->request->data['Section']['message'])) {
                $this->Session->setFlash(__('<div class="alert alert-success">Contato realizado. Obrigado!</div>'));
                return $this->redirect(array('action' => 'contato'));
            } else {
                return $this->redirect(array('action' => 'contato'));
            }
        }
    }

}
