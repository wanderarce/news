# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2014-08-07 00:02+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Plugin/Auditable/Controller/LoggersController.php:30
msgid "Log entry could not be find."
msgstr ""

#: Plugin/Auditable/Model/Behavior/AuditableBehavior.php:314
msgid "You need to define AuditableConfig::$Logger"
msgstr ""

#: Plugin/Auditable/Model/Behavior/AuditableBehavior.php:346
msgid "Can't save log entry for statement: \"%s'\""
msgstr ""

#: Plugin/Auditable/View/Helper/AuditorHelper.php:92
msgid "modified"
msgstr ""

#: Plugin/Auditable/View/Helper/AuditorHelper.php:101
msgid "created"
msgstr ""

#: Plugin/Auditable/View/Helper/AuditorHelper.php:105
msgid "deleted"
msgstr ""

#: Plugin/Auditable/View/Helper/AuditorHelper.php:115
msgid "undefined"
msgstr ""

#: Plugin/Auditable/View/Helper/AuditorHelper.php:116
msgid "nothing changed"
msgstr ""

#: Plugin/Auditable/View/Loggers/index.ctp:2
msgid "Logs"
msgstr ""

#: Plugin/Auditable/View/Loggers/index.ctp:11
msgid "Actions"
msgstr ""

#: Plugin/Auditable/View/Loggers/index.ctp:26
msgid "View"
msgstr ""

#: Plugin/Auditable/View/Loggers/view.ctp:2
msgid "Log"
msgstr ""

#: Plugin/Auditable/View/Loggers/view.ctp:4
msgid "Model"
msgstr ""

#: Plugin/Auditable/View/Loggers/view.ctp:9
msgid "Model ID"
msgstr ""

#: Plugin/Auditable/View/Loggers/view.ctp:14
msgid "Difference"
msgstr ""

#: Plugin/Auditable/View/Loggers/view.ctp:19
msgid "Created"
msgstr ""

