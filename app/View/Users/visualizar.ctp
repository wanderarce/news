<style type="text/css">

    body {
        padding: 30px 0px 60px;
    }
    .panel > .list-group .list-group-item:first-child {
        /*border-top: 1px solid rgb(204, 204, 204);*/
    }
    @media (max-width: 767px) {
        .visible-xs {
            display: inline-block !important;
        }
        .block {
            display: block !important;
            width: 100%;
            height: 1px !important;
        }
    }
    #back-to-bootsnipp {
        position: fixed;
        top: 10px; right: 10px;
    }


    .c-search > .form-control {
        border-radius: 0px;
        border-width: 0px;
        border-bottom-width: 1px;
        font-size: 1.3em;
        padding: 12px 12px;
        height: 44px;
        outline: none !important;
    }
    .c-search > .form-control:focus {
        outline:0px !important;
        -webkit-appearance:none;
        box-shadow: none;
    }
    .c-search > .input-group-btn .btn {
        border-radius: 0px;
        border-width: 0px;
        border-left-width: 1px;
        border-bottom-width: 1px;
        height: 44px;
    }


    .c-list {
        padding: 0px;
        min-height: 44px;
    }
    .title {
        display: inline-block;
        font-size: 1.7em;
        font-weight: bold;
        padding: 5px 15px;
    }
    ul.c-controls {
        list-style: none;
        margin: 0px;
        min-height: 44px;
    }

    ul.c-controls li {
        margin-top: 8px;
        float: left;
    }

    ul.c-controls li a {
        font-size: 1.7em;
        padding: 11px 10px 6px;   
    }
    ul.c-controls li a i {
        min-width: 24px;
        text-align: center;
    }

    ul.c-controls li a:hover {
        background-color: rgba(51, 51, 51, 0.2);
    }

    .c-toggle {
        font-size: 1.7em;
    }

    .name {
        font-size: 1.7em;
        font-weight: 700;
    }

    .c-info {
        padding: 5px 10px;
        font-size: 1.25em;
    }
</style>

<div class="">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading c-list">
                <span class="title">Jornalista</span>
            </div>

            <div class="row" style="display: none;">
                <div class="col-xs-12">
                    <div class="input-group c-search">
                        <input type="text" class="form-control" id="contact-list-search">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search text-muted"></span></button>
                        </span>
                    </div>
                </div>
            </div>

            <ul class="list-group" id="contact-list">
                
                <li class="list-group-item">    
                    <div class="col-xs-12 col-sm-5">
                        <?php echo $this->Html->image('/img/photo/' . $user['User']['id'] . '/' . $user['User']['photo'], array('class' => 'img-responsive img-circle')); ?>
                        </div>
                    <div class="col-xs-12 col-sm-7">
                        <span class="name"><?php echo h($user['User']['username']); ?></span><br/>
                        <span class="text-muted"><?php echo h($user['User']['email']); ?></span><br/>
                    </div>
                    <div class="clearfix"></div>
                </li>
                
            </ul>
        </div>
    </div>
</div>


<div class="related">
    <h3><?php echo __('Related News'); ?></h3>
    <?php if (!empty($user['News'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Title'); ?></th>
            </tr>
            <?php foreach ($user['News'] as $news): ?>
                <tr>
                    <td class="mobtek-item">
                        <?php echo $this->Html->link($news['title'], array('controller' => 'news', 'action' => 'visualizar', $news['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>