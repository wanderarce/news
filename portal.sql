-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Set-2014 às 04:27
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `portal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `albuns`
--

CREATE TABLE IF NOT EXISTS `albuns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `albuns`
--

INSERT INTO `albuns` (`id`, `title`, `created`, `modified`) VALUES
(1, 'teste', '2014-08-05 00:35:45', 1407191745),
(2, 'novo', '2014-08-12 22:38:11', 1407875891);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 4),
(4, NULL, 'Group', 2, NULL, 5, 10),
(5, 1, 'User', 1, NULL, 2, 3),
(6, 4, 'User', 2, NULL, 6, 7),
(7, 4, 'User', 4, NULL, 8, 9),
(8, NULL, 'User', 5, NULL, 11, 12),
(9, NULL, 'User', 10, NULL, 13, 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  `dir` varchar(45) NOT NULL,
  `banner` varchar(80) NOT NULL,
  `created` date NOT NULL,
  `modified` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `category`, `section_id`) VALUES
(1, 'Esporte', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administradores', '2014-06-18 02:32:51', '2014-06-18 02:32:51'),
(2, 'Moderadores', '2014-06-18 02:35:44', '2014-06-18 02:35:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) NOT NULL,
  `dir` varchar(250) DEFAULT NULL,
  `resume` text NOT NULL,
  `albun_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `images`
--

INSERT INTO `images` (`id`, `image`, `dir`, `resume`, `albun_id`, `created`, `modified`) VALUES
(1, 'music.jpg', NULL, 'ghgf', 1, '2014-08-05 00:36:41', '2014-08-12 21:00:14'),
(3, 'navicat.png', NULL, 'fgfd', 1, '2014-08-05 03:07:33', '2014-08-05 03:07:33'),
(4, 'sql.jpg', NULL, ';.çfg,fe,gplf,l', 1, '2014-08-12 20:59:48', '2014-08-12 20:59:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `responsible_id` int(11) NOT NULL,
  `model_alias` varchar(50) NOT NULL,
  `model_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `log_detail_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `types` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `logs`
--

INSERT INTO `logs` (`id`, `responsible_id`, `model_alias`, `model_id`, `type`, `log_detail_id`, `created`, `modified`) VALUES
(1, 3, 'News', 3, 2, 1, '2014-08-04 19:16:31', '2014-08-04 19:16:31'),
(2, 3, 'News', 1, 2, 2, '2014-08-06 15:49:06', '2014-08-06 15:49:06'),
(3, 3, 'News', 1, 3, 3, '2014-08-06 22:23:53', '2014-08-06 22:23:53'),
(4, 3, 'News', 2, 2, 4, '2014-08-07 01:16:41', '2014-08-07 01:16:41'),
(5, 1, 'News', 7, 2, 5, '2014-08-12 23:00:01', '2014-08-12 23:00:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_details`
--

CREATE TABLE IF NOT EXISTS `log_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `difference` text NOT NULL,
  `statement` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `log_details`
--

INSERT INTO `log_details` (`id`, `difference`, `statement`, `created`, `modified`) VALUES
(1, 'a:1:{s:8:"subtitle";a:2:{s:3:"old";s:9:"auditando";s:3:"new";s:12:"auditandodsd";}}', 'UPDATE `portal`.`news` SET `user_id` = 3, `category_id` = ''1'', `id` = 3, `title` = ''teste para auditoria'', `subtitle` = ''auditandodsd'', `body` = ''<p>auditoria</p>\\r\\n'', `fonte` = ''https://github.com/radig/auditable'', `modified` = ''2014-08-04 19:16:30''  WHERE `portal`.`news`.`id` = ''3''', '2014-08-04 19:16:31', '2014-08-04 19:16:31'),
(2, 'a:3:{s:7:"user_id";a:2:{s:3:"old";s:1:"1";s:3:"new";s:1:"3";}s:8:"subtitle";a:2:{s:3:"old";s:0:"";s:3:"new";s:36:"jdfjndsjnsdnmmdsokodsmokmmdmmdskmkmo";}s:4:"body";a:2:{s:3:"old";s:2095:"<p>A Pol&iacute;cia Federal divulgou o v&iacute;deo com o momento em que Roberlaine Patr&iacute;cia Alves, 28 anos, ent&atilde;o funcion&aacute;ria &nbsp;ligada ao Minist&eacute;rio da Sa&uacute;de &eacute; presa em flagrante recebendo R$ 100 mil como propina para liberar repasses federais para o Hospital do C&acirc;ncer.&nbsp;</p>\r\n\r\n<p>Segundo a Pol&iacute;cia Federal, na semana passada, o hospital depositou R$ 50 mil na conta da servidora e, nesta semana, ela veio buscar sete l&acirc;minas de cheque, que complementariam o pagamento da propina. Ela foi presa t&atilde;o logo recebeu os cheques. A mulher era funcion&aacute;ria contratada e atuava h&aacute; tr&ecirc;s anos na pasta, em Bras&iacute;lia.</p>\r\n\r\n<p>As investiga&ccedil;&otilde;es apontam que a propina era cobrada para liberar o repasse de verbas p&uacute;blicas federais oriundas de emendas parlamentares. A compra de um acelerador para radioterapia tamb&eacute;m foi alvo de cobran&ccedil;a, embora o hospital tivesse atendido os requisitos previstos em lei para obter os recursos e o aparelho</p>\r\n\r\n<p>As a&ccedil;&otilde;es da PF come&ccedil;aram em maio, quando o diretor do hospital em Campo Grande compareceu a pol&iacute;cia para relatar o crime. Com autoriza&ccedil;&atilde;o da Justi&ccedil;a, policiais passaram a monitorar a funcion&aacute;ria, que foi presa em flagrante ao ser filmada exigindo a propina do diretor do hospital.</p>\r\n\r\n<p>Nas imagens, ao ser dada a voz de pris&atilde;o, a mulher diz que h&aacute; mais pessoas envolvidas no esquema, mas em depoimento na Superintend&ecirc;ncia de Pol&iacute;cia Federal, ela alegou que atuou sozinha. A Pol&iacute;cia Federal acredita que deve concluir as investiga&ccedil;&otilde;es nos pr&oacute;ximos 15 dias. A mulher ser&aacute; indiciada por corrup&ccedil;&atilde;o passiva e lavagem de dinheiro.</p>\r\n\r\n<p>Participaram da coletiva de imprensa o superintendente da PF, Edgar Marcon, a delegada Kelly Bernardo e o presidente do Hospital do C&acirc;ncer, Carlos Coimbra.&nbsp;</p>\r\n\r\n<p><img alt="" src="C:\\Users\\Public\\Pictures\\Sample Pictures" /></p>\r\n";s:3:"new";s:10:"<p>a</p>\r\n";}}', 'UPDATE `portal`.`news` SET `user_id` = 3, `category_id` = ''1'', `id` = 1, `title` = ''Vídeo mostra funcionária presa ao receber propina'', `subtitle` = ''jdfjndsjnsdnmmdsokodsmokmmdmmdskmkmo'', `body` = ''<p>a</p>\\r\\n'', `fonte` = ''http://www.correiodoestado.com.br/noticias/video-mostra-funcionaria-presa-ao-receber-propina_219660/'', `modified` = ''2014-08-06 15:49:05''  WHERE `portal`.`news`.`id` = ''1''', '2014-08-06 15:49:06', '2014-08-06 15:49:06'),
(3, 'a:9:{s:2:"id";s:1:"1";s:7:"user_id";s:1:"3";s:5:"title";s:51:"Vídeo mostra funcionária presa ao receber propina";s:8:"subtitle";s:36:"jdfjndsjnsdnmmdsokodsmokmmdmmdskmkmo";s:5:"image";s:8:"conf.jpg";s:4:"body";s:10:"<p>a</p>\r\n";s:5:"fonte";s:100:"http://www.correiodoestado.com.br/noticias/video-mostra-funcionaria-presa-ao-receber-propina_219660/";s:11:"category_id";s:1:"1";s:9:"image_dir";N;}', 'DELETE `News` FROM `portal`.`news` AS `News`   WHERE `News`.`id` = 1', '2014-08-06 22:23:53', '2014-08-06 22:23:53'),
(4, 'a:2:{s:7:"user_id";a:2:{s:3:"old";s:1:"1";s:3:"new";s:1:"3";}s:8:"subtitle";a:2:{s:3:"old";s:0:"";s:3:"new";s:1:"k";}}', 'UPDATE `portal`.`news` SET `user_id` = 3, `category_id` = ''1'', `id` = 2, `title` = ''teste'', `subtitle` = ''k'', `body` = ''<p><img alt=\\"\\" src=\\"/cake/app/webroot/img/images/Jellyfish.jpg\\" style=\\"height:113px; width:150px\\" /></p>\\r\\n'', `fonte` = ''teste'', `modified` = ''2014-08-07 01:16:40''  WHERE `portal`.`news`.`id` = ''2''', '2014-08-07 01:16:41', '2014-08-07 01:16:41'),
(5, 'a:2:{s:7:"user_id";a:2:{s:3:"old";s:1:"3";s:3:"new";s:1:"1";}s:4:"body";a:2:{s:3:"old";s:14:"<p>teste</p>\r\n";s:3:"new";s:490:"<p>The replace() method searches a string for a specified value, or a&nbsp;<em>regular expression</em>, and returns a new string where the specified values are replaced.</p>\r\n\r\n<p>Read more about regular expressions in our&nbsp;<a href="http://www.w3schools.com/js/js_regexp.asp">RegExp Tutorial</a>&nbsp;and our&nbsp;<a href="http://www.w3schools.com/jsref/jsref_obj_regexp.asp">RegExp Object Reference</a>.</p>\r\n\r\n<p>This method does not change the original string.</p>\r\n\r\n<p>&nbsp;</p>\r\n";}}', 'UPDATE `portal`.`news` SET `user_id` = 1, `category_id` = ''1'', `id` = 7, `title` = ''esporte'', `subtitle` = ''esporte'', `body` = ''<p>The replace() method searches a string for a specified value, or a&nbsp;<em>regular expression</em>, and returns a new string where the specified values are replaced.</p>\\r\\n\\r\\n<p>Read more about regular expressions in our&nbsp;<a href=\\"http://www.w3schools.com/js/js_regexp.asp\\">RegExp Tutorial</a>&nbsp;and our&nbsp;<a href=\\"http://www.w3schools.com/jsref/jsref_obj_regexp.asp\\">RegExp Object Reference</a>.</p>\\r\\n\\r\\n<p>This method does not change the original string.</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n'', `fonte` = ''teste'', `modified` = ''2014-08-12 23:00:00''  WHERE `portal`.`news`.`id` = ''7''', '2014-08-12 23:00:01', '2014-08-12 23:00:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(45) NOT NULL,
  `image` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `fonte` varchar(255) NOT NULL,
  `category_id` varchar(45) NOT NULL,
  `image_dir` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `news`
--

INSERT INTO `news` (`id`, `user_id`, `title`, `subtitle`, `image`, `body`, `created`, `modified`, `fonte`, `category_id`, `image_dir`) VALUES
(2, 3, 'teste', 'k', '2014-02-08 14.47.36.jpg', '<p><img alt="" src="/cake/app/webroot/img/images/Jellyfish.jpg" style="height:113px; width:150px" /></p>\r\n', '2014-06-20 03:16:49', '2014-08-07 01:16:40', 'teste', '1', NULL),
(3, 3, 'teste para auditoria', 'auditandodsd', '', '<p>auditoria</p>\r\n', '2014-08-04 19:10:20', '2014-08-04 19:16:30', 'https://github.com/radig/auditable', '1', NULL),
(4, 1, 'teste', '', '', '<p><img alt="" src="/cake/app/webroot/img/images/Jellyfish.jpg" style="height:113px; width:150px" /></p>\r\n', '2014-06-20 03:16:49', '2014-06-20 22:18:20', 'teste', '1', NULL),
(5, 1, 'gggghkjjjh', 'gggghkjjjh', '', '<p>gggghkjjjhgggghkjjjhgggghkjjjhgggghkjjjhgggghkjjjhgggghkjjjhgggghkjjjh</p>\r\n', '2014-07-22 22:20:30', '2014-07-22 22:40:47', 'gggghkjjjhgggghkjjjhgggghkjjjh', '1', NULL),
(6, 1, 'esporte de', 'esporte run', '', '<p>teste</p>\r\n', '2014-07-22 22:26:47', '2014-07-22 23:01:25', 'teste', '2', NULL),
(7, 1, 'esporte', 'esporte', '', '<p>The replace() method searches a string for a specified value, or a&nbsp;<em>regular expression</em>, and returns a new string where the specified values are replaced.</p>\r\n\r\n<p>Read more about regular expressions in our&nbsp;<a href="http://www.w3schools.com/js/js_regexp.asp">RegExp Tutorial</a>&nbsp;and our&nbsp;<a href="http://www.w3schools.com/jsref/jsref_obj_regexp.asp">RegExp Object Reference</a>.</p>\r\n\r\n<p>This method does not change the original string.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2014-07-22 22:28:41', '2014-08-12 23:00:00', 'teste', '1', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `photo` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `photo_dir` varchar(45) DEFAULT NULL,
  `locked` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `group_id`, `created`, `modified`, `photo`, `email`, `photo_dir`, `locked`) VALUES
(1, 'admin', '06d0fb5a133894a22d7893e5514b2915f66630ff', 1, '2014-06-18 02:40:07', '2014-07-25 20:07:42', 'oracle.jpg', 'admin@admin.com', NULL, 0),
(2, 'editor', '06d0fb5a133894a22d7893e5514b2915f66630ff', 2, '2014-06-18 02:41:37', '2014-07-24 22:57:24', 'images.jpg', 'email@e.com', NULL, 0),
(3, 'wander', '06d0fb5a133894a22d7893e5514b2915f66630ff', 1, '2014-06-18 05:01:47', '2014-07-24 22:56:50', 'music.jpg', 'wander@teste.com', NULL, 0),
(4, 'teste', '65ab7214a614901765eeb53c4c6d02ee2045057f', 2, '2014-07-23 23:41:29', '2014-08-05 02:36:03', 'sql.jpg', 'teste@teste.com', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
