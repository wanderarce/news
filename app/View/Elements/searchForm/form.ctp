
<form action="<?php echo $url; ?>/news/search" controller="news" class="navbar-form navbar-left" role="search" id="NewsSearchForm" method="post" accept-charset="utf-8">

    <input type="hidden" name="_method" value="POST"/>
    <div class="form-group">    
        <input name="data[News][Search]" class="form-control" placeholder="Search" type="text" id="NewsSearch"/>
    </div>
    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
</form>