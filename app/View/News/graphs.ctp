
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<?php
$nnews = 0;
/*
 * 
  foreach ($categories as $cat) :
  echo '{name: "' . $cat . '",';
  echo 'data: [';
  $contador = 0;
  foreach ($news as $new):
  if ($new['Category']['category'] == $cat) {
  $contador++;
  }
  endforeach;
  echo $contador;
  echo ']}';
  endforeach;
 */
?>

<script type="text/javascript">
    $(function() {
        // Set up the chart
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'columngraph',
                type: 'column',
                margin: 75,
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
            },
            title: {
                text: 'Noticias por categoria'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [<?php
foreach ($categories as $cat) :
    echo '"' . $cat . '",';
endforeach;
?>]},
            yAxis: {
                min: 0,
                title: {text: 'Total de notícias'}
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            series: [<?php
foreach ($categories as $cat) :
    echo '{name: "' . $cat . '",';
    echo 'data: [';
    $contador = 0;
    foreach ($news as $new):
        if ($new['Category']['category'] == $cat) {
            $contador++;
        }
    endforeach;
    echo $contador;
    $nnews += $contador;
    echo ']},';
endforeach;
?>
            ]
        });

        function showValues() {
            $('#R0-value').html(chart.options.chart.options3d.alpha);
            $('#R1-value').html(chart.options.chart.options3d.beta);
        }

        // Activate the sliders
        $('#R0').on('change', function() {
            chart.options.chart.options3d.alpha = this.value;
            showValues();
            chart.redraw(false);
        });
        $('#R1').on('change', function() {
            chart.options.chart.options3d.beta = this.value;
            showValues();
            chart.redraw(false);
        });

        showValues();
    });

    $(function() {
        $('#pie').highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Noticias por Categoria'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                    type: 'pie',
                    name: 'Porcentagem: ',
                    data: [
<?php
$tnn = 100/$nnews;
foreach ($categories as $cat) :
    echo "['" . $cat . "',";
    $cont = 0;
    foreach ($news as $new):
        if ($new['Category']['category'] == $cat) {
            $cont++;
        }
    endforeach;
    echo $cont*$tnn."],";
endforeach;
?>
                        ['Margem de erro', 0.1]
                    ]
                }]
        });
    });

</script>        
<style>
    #columngraph{height: 500px;

    }
    #pie{height: 500px;
    }

</style>

<div id="columngraph"></div>
<div id="sliders">
    <table>
        <tr><td>Alpha Angle</td><td><input id="R0" class="form-control" type="range" min="0" max="45" value="15"/> <span id="R0-value" class="value"></span></td></tr>
        <tr><td>Beta Angle</td><td><input id="R1" class="form-control" type="range" min="0" max="45" value="15"/> <span id="R1-value" class="value"></span></td></tr>
    </table>

</div>

<div id="pie"></div>
