<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $helperss = array('Time');
    public $components = array('Paginator', 'Email');
    public $paginate = array(
        'limit' => 10,
    );

    public function index() {
        $this->Paginator->settings = $this->paginate;
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    public function expediente() {
        $this->layout = 'mobtek';
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-success">The user could not be saved. Please, try again.</div>'));
            }
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    public function edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('<div class="alert alert-error">Invalid user</div>'));
        }
        if ($this->request->is(array('post', 'put'))) {
            
            
            $this->User->find('first',array('conditions' => array('User.' . $this->User->primaryKey => $id)));
            
            
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The user has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-warning">The user could not be saved. Please, try again.</div>'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }
    
    public function newPassword($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('<div class="alert alert-error">Invalid user</div>'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The user has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-warning">The user could not be saved. Please, try again.</div>'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('<div class="alert alert-warning">Invalid user.</div>'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('<div class="alert alert-success">The user has been deleted.</div>'));
        } else {
            $this->Session->setFlash(__('<div class="alert alert-error">The user could not be deleted. Please, try again.</div>'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function login() {

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirect());
            } else {
                $this->Session->setFlash('<div class="alert alert-danger"><center>Usu&aacute;rio ou Senha inválido. Tente novamente!</center></div>');
            }
        }
    }

    //ACAO DE LOGOUT
    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function visualizar($id = null) {
        $this->layout = 'mobtek';
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    //RECUPERANDO SENHA
    public function recoveryPassword() {
        if (!empty($this->data)) {
            $user = $this->User->findByLogin($this->data['User']['login']);
            if (empty($user)) {
                $this->Session->setFlash('<div class="alert alert-danger text-center">Desculpe, login inválido. Adicione um login existente. </div>');
                $this->redirect($this->referer());
            } else {
                $user['User']['password'] = microtime(true) + (3600 * 48);
                $this->set('user',$user);
                $this->sendPassword();
                $this->Session->setFlash('<div class="alert alert-succes text-center">Password reset instructions have been sent to your email address.
						You have 24 hours to complete the request.</div>');
                $this->redirect('/users/login');
            }
        }
    }

    //ENVIANDO EMAIL PARA SENHA
    public function sendPassword($id = null) {
        if (!empty($id)) {
            $this->User->id = $id;
            $user = $this->User->read();
            $this->Email->to = $user['User']['email'];
            $this->Email->subject = 'Password Changed - DO NOT REPLY';
            $this->Email->replyTo = 'contato@mobtek.com';
            $this->Email->from = 'Do Not Reply <do-not-reply@example.com>';
            $this->Email->template = 'password_reset_success' . $user['User']['password'] . '.';
            $this->Email->sendAs = 'both';
            $this->set('user', $user);
            $this->Email->send();
            return true;
        }
        return false;
    }

}