<div class="categories view">
    <h2><?php echo h($category['Category']['category']); ?></h2>

    <div class="row">
        <div class="[col-xs-12]">
            <ul class="event-list">
                <?php
                foreach ($category['News'] as $news):
                    $datas = date_create($news['created']);
                    ?>
                    <li>
                        <time datetime="<?php echo date_format($datas, 'H:i:s d/m/Y'); ?>">
                            <?php ?>
                            <span class="day"><?php echo date_format($datas, 'd'); ?></span>
                            <span class="month"><?php echo date_format($datas, 'M'); ?></span>
                            <span class="year"><?php echo date_format($datas, 'Y'); ?></span>
                            <span class="time"><?php echo date_format($datas, 'H:i:s'); ?></span>
                        </time>
                        <div class="info">
                            <h4 class="title"> 
                                <?php echo $this->Html->link($news['title'], array('controller' => 'news', 'action' => 'visualizar', $news['id'])); ?>
                            </h4>
                            <p class="desc"> <?php echo $news['subtitle']; ?> </p>
                        </div>
                    </li>
                    <?php
                endforeach;
                ?>
            </ul>
        </div>
    </div>
</div>
