<div class="rows">
    <h5>
        <b><?php echo __('News'); ?>: 
            <?php echo $this->Html->link($news['Category']['category'], array('controller' => 'categories', 'action' => 'view', $news['Category']['id'])); ?>
            <span class="pull-right">
                <?php
                $datas = date_create($news['News']['created']);
                echo date_format($datas, 'H:i:s d M Y');
                ?>
            </span></b>
    </h5>

    <h2><center><?php echo h($news['News']['title']); ?></center></h2>

    <h4><center><?php echo h($news['News']['subtitle']); ?></center></h4>

    <p>
        Publicado por <b>
            <?php
            echo $this->Html->link($news['User']['username'], array('controller' => 'users', 'action' => 'visualizar', $news['User']['id']));
            ?></b>
    </p>    

    <div class="rows">
        <?php
        echo $this->Html->image('/img/archive/' . $news['News']['id'] . '/' . $news['News']['archive'], array('class' => 'img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12', 'alt' => $news['News']['archive']));
        ?>
        <?php echo $news['News']['body']; ?>
        &nbsp;
    </div>

    <div><b><?php echo __('Fonte'); ?></b>:   <?php echo h($news['News']['fonte']); ?></div>

</div>