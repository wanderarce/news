<style type="text/css">
    a.list-group-item {
        height:auto;
        min-height: 100px;
    }
    a.list-group-item.active small {
        color:#fff;
    }
</style>
<div class="">
    <div class="row">
        <div class="well">
            <h1 class="text-center">Noticias</h1>
            <div class="list-group">

                <?php foreach ($news as $new): ?>
                    <a href="#" class="list-group-item">
                        
                        <?php if (!empty($new['News']['image'])) { ?>
                        <div class="media col-md-3">
                            <figure class="pull-left">
                                <img class="media-object img-rounded img-responsive"  src="http://localhost/mobtek/news/img/image/1/<?php echo $new['News']['image']; ?>" alt="<?php echo $new['News']['title']; ?>" >
                            </figure>
                        </div>
                    <?php } ?>
                        <div class="col-md-6">
                            <h4 class="list-group-item-heading"> <?php echo h($new['News']['title']); ?> </h4>
                            <p class="list-group-item-text"> 
                                <?php echo h($new['News']['subtitle']); ?>
                            </p>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>