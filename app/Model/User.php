<?php

App::uses('AppModel', 'Model');

class User extends AppModel {

//APOS CRIAR O USUARIO, FAZ A CRIPTOGRAFIA DA SENHA

    public function beforeSave($options = array()) {
        if (!empty($this->data['User']['password'])) {
            if (isset($this->data['User']['password'])) {
                $password = &$this->data['User']['password'];
                $password = AuthComponent::password($password);
            }
            if (isset($this->data['User']['confirm'])) {
                $confirm = &$this->data['User']['confirm'];
                $confirm = AuthComponent::password($confirm);
            }
            if ($password == $confirm)
                return parent::beforeSave($options);
        }
    }

    public $actsAs = array(
        'Acl' => array('type' => 'requester'),
        'Upload.Upload' => array(
            'photo' => array(
                'fields' => array(
                    'dir' => 'img_dir'
                ),
                'rule' => array('mimeType' => array('image/png', 'image/jpg', 'image/jpeg')),
                'message' => 'Invalid mime type',
                'thumbnailSizes' => array(
                    'big' => '639x423',
                    'small' => '130x130',
                    'thumb' => '90x82'
                ),
                'thumbnailMethod' => 'php',
                //necessario informar o path para que seja salvo corretamente.
                'path' => '{ROOT}webroot{DS}img{DS}{field}{DS}',
                'deleteOnUpdate' => true
            )
        )
    ); //ARO
    public $validate = array(
        'username' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Este campo é obrigatório!!'
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'), 'message' => 'O email não pode ficar em branco!'
            ),
            'unique' => array(
                'rule' => array('isUnique'), 'message' => 'Email já cadastrado!'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a senha!!'
            ),
            'min' => array(
                'rule' => array('minLength', 6),
                'message' => 'Minimo 6 caracteres!!!'
            ),
        ),
        'confirm' => array(
            'required' => array(
                'rule' => array('checkPasswords'),
                'message' => 'Senhas diferentes!!!'
            ),
        ),
        'group_id' => array(
            'numeric' => array(
                'rule' => array('numeric')
//'message' => 'Your custom message here'
            ),
        ),
    );
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id'
        )
    );
    public $hasMany = array(
        'News' => array(
            'className' => 'News',
            'foreignKey' => 'user_id',
            'dependent' => false
        )
    );

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }

        if (isset($this->data[$this->alias]['group_id'])) {
            $groupId = $this->data[$this->alias]['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        return $groupId ? array('Group' => array('id' => (int) $groupId)) : null;
    }

    function checkPasswords($data) {
        if ($this->data['User']['password'] === $this->data['User']['confirm']) {
            return true;
        }return false;
    }

}
