<?php

App::uses('AppModel', 'Model');

class Category extends AppModel {

    public $validate = array(
        'category' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Is required',
            ),
        ),
        'color' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            ),
        ),
    );
    public $hasMany = array(
        'News' => array(
            'className' => 'News',
            'foreignKey' => 'category_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public $belongsTo = array(
        'Section' => array(
            'className' => 'Section',
            'foreignKey' => 'section_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
