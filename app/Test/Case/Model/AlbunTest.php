<?php
App::uses('Albun', 'Model');

/**
 * Albun Test Case
 *
 */
class AlbunTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.albun',
		'app.image'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Albun = ClassRegistry::init('Albun');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Albun);

		parent::tearDown();
	}

}
