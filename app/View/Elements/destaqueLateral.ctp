<style type="text/css">
    a.mobtek-item{
        text-decoration: none;
        color: #262626 !important;

    }
    
    figure{width: 15% !important;
           margin: 2px 3px !important;
    }
    panel-default>.panel-heading{background: #FFFF99;
    color: #FFFFFF !important;}
    .panel-default{border-color: #B0B0B0;}
    
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="text-center">Destaque Lateral</h2>
    </div>
    <?php $news = $this->requestAction('news/destaque/2/sort:created/direction:asc/limit:10'); ?>
    <?php foreach ($news as $new): ?>
        <h4 class="panel-title"> 
            <?php echo $this->Html->link($new['News']['title'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id']), array('class' => 'mobtek-item')); ?>
        </h4>
        <h5> 
               <?php echo $this->Html->link($new['News']['subtitle'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id']), array('class' => 'mobtek-item')); ?>
        </h5>
        <hr>
    <?php endforeach; ?>
</div>