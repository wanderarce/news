<?php $sections = $this->requestAction('sections/listar/sort:created/direction:asc'); ?>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $url; ?>">Mobtek News</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <!-- Menu -->
            <ul class="nav navbar-nav">

                <?php foreach ($sections as $section): ?>

                    <?php if (!empty($section['Category'])) { ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $section['Section']['section']; ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <?php foreach ($section['Category'] as $categories): ?>
                                    <li><?php echo $this->Html->link($categories['category'], array('controller' => 'categories', 'action' => 'view', $categories['id'],)); ?></li>
                                <?php endforeach; ?>

                            </ul>
                        </li>
                    <?php } else { ?>
                        <li><?php echo $this->Html->link($section['Section']['section'], array('controller' => 'sections', 'action' => 'view', $section['Section']['id'])); ?></li>
                    <?php } ?>
                <?php endforeach; ?>
                        <li><?php echo $this->Html->link(__('Contato'), array('controller' => 'sections', 'action' => 'contato')); ?></li>
                        <li><?php echo $this->Html->link(__('Expediente'), array('controller' => 'users', 'action' => 'expediente')); ?></li>


            </ul>
      

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
