<style type="text/css">
        a.list-group-item {
            height:auto;
            min-height: 100px;
        }
        a.list-group-item.active small {
            color:#fff;
        }
    </style>
    <div class="">
        <div class="row">
            <div class="well">
                <h1 class="text-center">Noticias</h1>
                <div class="list-group">

                    <?php foreach ($news as $news): ?>
                        <a href="#" class="list-group-item">
                            <div class="media col-md-3">
                                <figure class="pull-left">
                                    <img class="media-object img-rounded img-responsive"  src="http://placehold.it/350x250" alt="placehold.it/350x250" >
                                </figure>
                            </div>
                            <div class="col-md-6">
                                <h4 class="list-group-item-heading"> <?php echo h($news['News']['title']); ?> </h4>
                                <p class="list-group-item-text"> 
                                    <?php echo h($news['News']['subtitle']); ?>
                                </p>
                            </div>
                        </a>
                    <?php endforeach; ?>
                    <div class="clearfix">
                        <p>
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>	
                        </p>
                    </div>
                    <ul class="pagination pull-right">
                        <li><?php
                            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                            echo $this->Paginator->numbers(array('separator' => ''));
                            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>