
<div class="actions">
	<b class=" glyphicon glyphicon-edit">
        <?php echo $this->Html->link(__('Edit Banner'), array('action' => 'edit', $banner['Banner']['id'])); ?> </b>
        <b class=" glyphicon glyphicon-remove">
        <?php echo $this->Form->postLink(__('Delete Banner'), array('action' => 'delete', $banner['Banner']['id']), array('confirm'=>  __('Are you sure you want to delete # %s?', $banner['Banner']['id']))); ?> </b>
        <b class=" glyphicon glyphicon-list">
        <?php echo $this->Html->link(__('List Banners'), array('action' => 'index')); ?> </b>
		<b class=" glyphicon glyphicon-plus">
        <?php echo $this->Html->link(__('New Banner'), array('action' => 'add')); ?> </b>
</div>
<div class="banners view">
<h2><?php echo __('Banner'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($banner['Banner']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($banner['Banner']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Banner'); ?></dt>
		<dd>
			<?php echo $this->Html->image('/img/banner/'.$banner['Banner']['id'].'/big_'.$banner['Banner']['banner']); ?>
                    
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($banner['Banner']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($banner['Banner']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>