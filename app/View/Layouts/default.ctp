<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>

        <link href="/news/cake.ico" type="image/x-icon" rel="icon">

        <?php
        echo $this->Html->css(array('bootstrap.min.css?v=1', 'bootstrap.css?v=1', 'bootstrap.css.map?v=1',
            'bootstrap-theme.css?v=1', 'bootstrap-theme.min.css', 'bootstrap-theme.css.map?v=1')) . PHP_EOL;

        //echo $this->Html->css(array('bootstrap','bootstrap.min','bootstrap-theme','bootstrap-theme.min'));
        ?>
        <?php
        echo $this->Html->script(array('jquery-1.11.1.min.js', 'bootstrap.min', 'bootstrap',
            'ckfinder/ckfinder.js', 'ckeditor/ckeditor.js'));
        ?>
        
        <script type="text/javascript">
            var ckEditor = CKEDITOR.replace('texto');
        </script>
        <style type="text/css">
        .news-icone{width: 5%;
                    
        }

        footer{margin-bottom: 0;}
        </style>
        
    </head>
    <body>
        <main>
            <header class="col-md-12  navbar navbar-inverse navbar-static-top">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo $url; ?>">
                        <?php echo $this->Html->image('ico.png', array('alt' => 'News', 'class' => 'news-icone')); ?>
                        News
                    </a>
                </div>
                <div class="navbar-header pull-right">
                    <a class="navbar-brand" href="<?php echo $url . '/sair'; ?>">Sair</a>
                </div>
                <div class="navbar-brand pull-right  "> 
                    <?php if($this->Session->read('Auth.User.username')!=''){
                     echo $this->Html->link(__($this->Session->read('Auth.User.username')), array('controller'=>'users','action' => 'view', $this->Session->read('Auth.User.id'))); 
                    };?>
                </div>
            </header>

            <?php if (($this->Session->read('Auth.User.group_id'))): ?>
                <nav>
                    <?php echo $this->element('menuadmin'); ?>
                </nav>
            <?php endif; ?>
            <article class="col-sm-9 col-md-9">
                <div class="">
                    <?php echo $this->Session->flash(); ?>

                    <?php echo $this->fetch('content'); ?>   

                </div>
            </article>

            <footer class="col-md-12 navbar-inverse">
                <center>
                    <style type="text/css">.immob{width: 5%;}</style>
                    <a href="http://www.mobtek.com.br" target="_blank">
                        <?php
                        echo $this->Html->image('http://www.mobtek.com.br/mobimage/mobtek-logo.png', array('class' => 'immob'));
                        ?>
                    </a>
                </center>
            </footer>
        </main>
        <?php echo $this->element('sql_dump'); ?>

    </body>
</html>
