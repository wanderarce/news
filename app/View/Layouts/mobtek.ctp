<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title><?php echo $site; ?></title>
        <meta name="generator" content="Mobtek" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">

        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <link href="/news/cake.ico" type="image/x-icon" rel="icon">

        <!-- CSS code from Bootply.com editor -->

        <style type="text/css">
            .news-icone{
                width: 5%; 
            }
            .container {
                margin: 0 auto;
                max-width: 960px;
            }
            .mobtek-faixa{
                background-color: #eee;
                border-radius: 6px;
            }
            .mobtek-banner{
                width: 99%;
                margin: 3%;
            }
            .MOBIMAGE{
                width: 120%; 
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                var sideslider = $('[data-toggle=collapse-side]');
                var sel = sideslider.attr('data-target');
                var sel2 = sideslider.attr('data-target-2');
                sideslider.click(function (event) {
                    $(sel).toggleClass('in');
                    $(sel2).toggleClass('out');
                });
            });</script>
    </head>

    <!-- HTML code from Bootply.com editor -->

    <body  >
        <div class="container">

            <header class="nav">
                <h1 class="pull-left col-lg-8"><?php echo $site; ?><br>
                    <small style="margin-left: 10%"><?php echo $slogan; ?>
                    </small>
                </h1>
                <div class="pull-right"><?php echo $this->element('searchForm/form'); ?></div>
                <div class="pull-right">
                    <button id="maior" class="btn"><b>A+</b></button>
                    <button id="menor" class="btn"><b>A-</b></button>
                </div>
            </header>		


            <div class="container">
                <?php echo $this->element('menu'); ?>
            </div>
            <div>
                <?php echo $this->element('banners', array('type' => '4')); ?>
            </div>
            <div class="container">
                <div>
                    <?php echo $this->element('banners', array('type' => '3')); ?>
                </div>

                <div class="mobtek-faixa text-center">
                    <?php echo $this->element('destaqueFaixa'); ?>
                </div>

                <div class="row">

                    <div class="col-lg-8">
                        <div id="conteudo">
                            <?php echo $this->Session->flash(); ?>

                            <div>
                                <?php echo $this->element('banners', array('type' => '5')); ?>
                            </div>

                            <?php echo $this->fetch('content'); ?> 
                            <?php // echo $this->element('geral'); ?>
                        </div>
                    </div>

                    <div class="col-lg-4">

                        <div>
                            <?php echo $this->element('banners', array('type' => '2')); ?>
                        </div>

                        <div>
                            <?php echo $this->element('destaqueLateral'); ?>
                        </div>

                        <div>
                            <?php echo $this->element('banners', array('type' => '2')); ?>
                        </div>

                        <div>
                            <?php echo $this->element('ultimas'); ?>
                        </div>

                        <div>
                            <?php echo $this->element('banners', array('type' => '2')); ?>
                        </div>

                        <div>
                            <?php echo $this->element('visualizadas'); ?>
                        </div>

                        <div>
                            <?php echo $this->element('banners', array('type' => '2')); ?>
                        </div>

                    </div>
                </div>

                <hr>

            </div> <!-- /container -->


            <footer class="container">

                <div>
                    <?php echo $this->element('banners', array('type' => '6')); ?>
                </div>

                <div class="row">
                    <div class="col-lg-11">
                        <center>
                            <?php echo $this->Html->image('ico.png', array('alt' => 'News', 'class' => 'news-icone')); ?>
                            <b><a href="<?php echo $url; ?>"><?php echo $site; ?></a></b>
                            <p class="footertext">Copyright 2014</p>
                        </center>
                    </div>
                    <div class="col-lg-1">
                        <a href="http://www.mobtek.com.br" target="_blank" class="pull-right">
                            <?php
                            echo $this->Html->image('http://www.mobtek.com.br/mobimage/mobtek-logo.png', array('class' => 'MOBIMAGE'));
                            ?>
                        </a>
                    </div>
                </div>
            </footer>

        </div>
        <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type='text/javascript' src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

        <!-- JavaScript jQuery code from Bootply.com editor  -->

        <script type='text/javascript'>

            $(document).ready(function () {



            });</script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-40413119-1', 'bootply.com');
            ga('send', 'pageview');</script>
        <!-- Quantcast Tag -->
        <script type="text/javascript">
            var _qevents = _qevents || [];
            (function () {
                var elem = document.createElement('script');
                elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
                elem.async = true;
                elem.type = "text/javascript";
                var scpt = document.getElementsByTagName('script')[0];
                scpt.parentNode.insertBefore(elem, scpt);
            })();
            _qevents.push({
                qacct: "p-0cXb7ATGU9nz5"
            });
            $(document).ready(function () {
                $("#maior").click(function () {
                    var title = $("h1").css('font-size');
                    var subtitle = $("h2,h3,h4,h5").css('font-size');
                    var paragraph = $("p").css('font-size');
                    title = title.replace('px', '');
                    title = parseInt(title) + 1;
                    subtitle = subtitle.replace('px', '');
                    subtitle = parseInt(subtitle) + 1;
                    paragraph = paragraph.replace('px', '');
                    paragraph = parseInt(paragraph) + 1;
                    $("h1").animate({"font-size": title + 'px'});
                    $("h2,h3,h4,h5").animate({"font-size": subtitle + 'px'});
                    $("p").animate({"font-size": paragraph + 'px'});
                });
                $("#menor").click(function () {
                    var title = $("h1").css('font-size');
                    var subtitle = $("h2,h3,h4,h5").css('font-size');
                    var paragraph = $("p").css('font-size');
                    title = title.replace('px', '');
                    title = parseInt(title) - 1;
                    subtitle = subtitle.replace('px', '');
                    subtitle = parseInt(subtitle) - 1;
                    paragraph = paragraph.replace('px', '');
                    paragraph = parseInt(paragraph) - 1;
                    $("h1").animate({"font-size": title + 'px'});
                    $("h2,h3,h4,h5").animate({"font-size": subtitle + 'px'});
                    $("p").animate({"font-size": paragraph + 'px'});
                });
            });

        </script>
    </body>
</html>