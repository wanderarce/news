<?php

App::uses('Model', 'Model');
App::uses('AuditableConfig', 'Auditable.Lib');

class AppModel extends Model {
	public function __construct($id = false, $table = null, $ds = null) {
		if(get_class($this) !== 'Logger' && empty(AuditableConfig::$Logger)) {
			// Caso deseje usar o modelo padrão, utilize como abaixo, caso contrário você pode usar qualquer modelo
			AuditableConfig::$Logger = ClassRegistry::init('Auditable.Logger', true);
		}

		parent::__construct($id, $table, $ds);
	}

}
