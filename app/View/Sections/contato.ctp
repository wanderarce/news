<style>
    .rows{margin: 2px !important;}
</style>
<div class="categories form">
    <p>Utilize o formulário abaixo para entrar em contato.</p>
    <?php echo $this->Form->create('Section', array('action' => 'contato')); ?>
    <div class="rows">
        <?php echo $this->Form->input('Section.name', array('label' => 'Nome', 'maxlength' => 100, 'size' => 40, 'class' => 'form-control')); ?>
    </div>
    <div class="rows">
        <?php echo $this->Form->input('Section.company', array('label' => 'Empresa', 'maxlength' => 100, 'size' => 40, 'class' => 'form-control')); ?>
    </div>
    <div class="rows">
        <?php echo $this->Form->input('Section.email', array('label' => 'Email', 'maxlength' => 100, 'size' => 40, 'class' => 'form-control')); ?>
    </div>
    <div class="rows">
        <?php echo $this->Form->input('Section.message', array('label' => 'Motivo do contato', 'cols' => 50, 'rows' => 10, 'class' => 'form-control')); ?>
    </div>
    <br>
    <div class="rows"><?php echo $this->Form->submit('Enviar e-Mail', array('class' => 'btn btn-primary')); ?></div>
    <div class="rows"><?php echo $this->Form->end(); ?></div>

</div>