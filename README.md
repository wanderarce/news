#MobtekNews


Portal de noticias, com foco na usabilidade e midias sociais.

MobtekNews gerenciador de noticias.

##Possibilidades
----------------

*[ ] News com visitantes:.
    Noticias.
    Artigos.
    Colunas.
    Videos via Youtube.
    Classificados.
    Institucional.

*[ ] Publicidade via banners.

*[ ] Fotos.

*[ ] Eventos (calendario).

*[ ] Sua vez (publicação dos leitores).

*[ ] Sistema de busca.

*[ ] Expediente.

*[ ] Sistema de gerenciamento de usuarios(com log de atividades no sistema).

*[ ] Estatisticas (de publicação e visualização).

*[ ] Agendamento de publicações.

*[ ] RSS.

*[ ] versão impressa.

*[ ] Radio web.

##Implementação
------------

*[ ] CakePHP.

*[ ] MySQL.

*[ ] Git.

*[ ] Ckeditor/Cfinder.