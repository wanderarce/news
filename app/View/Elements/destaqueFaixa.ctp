<style type="text/css">
    div.clear
    {
        clear: both;
    }

    div.product-chooser{

    }

    div.product-chooser.disabled div.product-chooser-item
    {
        zoom: 1;
        filter: alpha(opacity=60);
        opacity: 0.6;
        cursor: default;
    }

    div.product-chooser div.product-chooser-item{
        padding: 11px;
        border-radius: 6px;
        cursor: pointer;
        position: relative;
        border: 1px solid #efefef;
        margin-bottom: 10px;
        margin-left: 10px;
        margin-right: 10x;
    }

    div.product-chooser div.product-chooser-item.selected{
        border: 4px solid #428bca;
        background: #efefef;
        padding: 8px;
        filter: alpha(opacity=100);
        opacity: 1;
    }

    div.product-chooser div.product-chooser-item img{
        padding: 0;
    }

    div.product-chooser div.product-chooser-item span.title{
        display: block;
        margin: 10px 0 5px 0;
        font-weight: bold;
        font-size: 12px;
    }

    div.product-chooser div.product-chooser-item span.description{
        font-size: 12px;
    }

    div.product-chooser div.product-chooser-item input{
        position: absolute;
        left: 0;
        top: 0;
        visibility:hidden;
    }
</style>
<script type="text/javascript">
    $(function() {
        $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function() {
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);

        });
    });
</script>

<div class="">
    <h2>Destaques em faixa</h2>
    <div class="row form-group product-chooser">

        <?php $news = $this->requestAction('news/destaque/3/sort:created/direction:asc/limit:10'); ?>
        
        <?php foreach ($news as $new): ?>
            <?php if (!empty($new["News"]["archive"])) { ?>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="product-chooser-item">
                                <?php
                                echo $this->Html->image('/img/archive/' . $new['News']['id'] . '/' . $new['News']['archive'], array('class' => 'img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12', 'alt' => $new['News']['archive']));
                                ?>
                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                            <span class="title"><?php echo $this->Html->link($new['News']['title'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id'])); ?></span>
                            <span class="description"><?php echo $this->Html->link($new['News']['subtitle'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id'])); ?></span>
                            
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php } ?>
        <?php endforeach; ?>

    </div>
</div>