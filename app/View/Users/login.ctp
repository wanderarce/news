<style>

    .form-control, input[type=submit].btn-block, input[type=reset].btn-block, input[type=button].btn-block{
        width:90.2% !important;
        float: none !important;
        margin: 0 auto !important;
    }
    article{width:100% !important;
            
    }
    .panel{margin: 0 auto !important;
           margin-top: 15% !important;
           width:40% !important;

    }
</style>

<div class="panel panel-default text-center">
    <div class="panel-heading">Log in</div>
    <?php echo $this->Form->create('User', array('action' => 'login', 'class' => '')); ?>

    <div class="form-group">
        <?php echo $this->Form->input('username', array('label' => false, 'class' => 'login form-control', 'placeholder' => 'username')); ?>
    </div>
    <div class="form-group">            
        <?php echo $this->Form->input('password', array('label' => false, 'class' => 'form-control', 'placeholder' => 'password')); ?>
    </div>
    <div class="clearfix"></div>
    <div class="form-group">            
        <?php //echo $this->Form->end('Log in', array('type'=>'submit','id'=>'btn-login','class' => 'btn btn-lg btn-success btn-block')); ?>
        <input type="submit" id="btn-login" class="btn btn-lg btn-success btn-block " value="Log in">
    </div>
    <hr>
    <div class="form-group">
        <!--a fazer -->
        <?php echo $this->Html->link("Reset password", array('controller' => 'users', 'action' => 'recoveryPassword')); ?>
    </div>
</div>
