<div class="row">

    <button class="btn-link">
        <b class=" glyphicon glyphicon-list">
            <?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?>
        </b>
    </button>
    <button class="btn-link">
        <b class=" glyphicon glyphicon-list">
            <?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?>
        </b>
    </button>

    <button class="btn-link">
        <b class=" glyphicon glyphicon-plus">
            <?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?>
        </b>
    </button>
    <button class="btn-link">
        <b class=" glyphicon glyphicon-list">
            <?php echo $this->Html->link(__('List News'), array('controller' => 'news', 'action' => 'index')); ?>
        </b>
    </button>
    <button class="btn-link">
        <b class=" glyphicon glyphicon-plus">
            <?php echo $this->Html->link(__('Group'), array('controller' => 'groups', 'action' => 'add')); ?>
        </b>
    </button>
    <button class="btn-link">
        <b class=" glyphicon glyphicon-plus">
            <?php echo $this->Html->link(__('New News'), array('controller' => 'news', 'action' => 'add')); ?>
        </b>
    </button>
</div>


<div class="users form">

    <?php echo $this->Form->create('User',array('type' => 'file')); ?>
    <fieldset>
        <legend><?php echo __('Edit User'); ?></legend>
       
        <div class="row">
            <div class="col-md-11">
       
            <?php echo $this->Form->create('User'); ?>
        <div class="row">
            <div class="col-md-6">
                <?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php echo $this->Form->input('username', array('class' => 'form-control', 'disabled')); ?>
            </div>
        </div>
        <div class="row">
           <div class="col-md-6">
                <?php echo $this->Form->input('password', array('type'=>'password','class' => 'form-control', 'value'=>'')); ?>
            </div>
        </div>                
        <div class="row">
            <div class="col-md-6 ">
                <?php echo $this->Form->input('confirm', array('type'=>'password','class' => 'form-control')); ?>
            </div>
        </div>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>