
<div class="rows">
	<button class="btn-link">
        <b class=" glyphicon glyphicon-remove">
    		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')),  array('confirm'=> __('Are you sure you want to delete # %s?', $this->Form->value('Group.id')))); ?>
    	</b>
    </button>

    <button class="btn-link">
    
    	<b class=" glyphicon glyphicon-list">
    		<?php echo $this->Html->link(__('Groups'), array('action' => 'index')); ?>
		</b>
    </button>

    <button class="btn-link">
    	<b class=" glyphicon glyphicon-list">
    	<?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?>
		</b>
    </button>

    <button class="btn-link">
    	<b class=" glyphicon glyphicon-plus">
    	<?php echo $this->Html->link(__('User'), array('controller' => 'users', 'action' => 'add')); ?> </b>
    </button>
</div>

<div class="groups form">
<?php echo $this->Form->create('Group'); ?>
	<fieldset>
		<legend><?php echo __('Edit Group'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('class' => 'form-control'));
	?>
	</fieldset>
	<hr>
<?php echo $this->Form->end(__('Submit')); ?>
</div>