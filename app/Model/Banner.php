<?php

App::uses('AppModel', 'Model');

/**
 * Banner Model
 *
 */
class Banner extends AppModel {

    public $actsAs = array(
        'Upload.Upload' => array(
            'title',
            'banner' => array(
                'fields' => array(
                    'dir' => 'img_dir',
                    'type' => 'image_type',
                    'size' => 'image_size',
                ),
                'rule'=>array('mimeType'=>array('image/gif','image/png','image/jpg','image/jpeg','application/x-shockwave-flash')),
                'message'=>'Invalid mime type',
                'thumbnailSizes' => array(
                    'big' => '234x60',
                    'small' => '130x130',
                    'thumb' => '90x82'
                ),
                'thumbnailMethod' => 'php',
                //necessario informar o path para que seja salvo corretamente.
                'path' => '{ROOT}webroot{DS}img{DS}{field}{DS}',
                'deleteOnUpdate' => true
            )
        )
   );
    
    public $validate = array(
        'title' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'tipo' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'dir' => array(
            //'notEmpty' => array(
              //  'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            //),
        ),
        
    );

}
