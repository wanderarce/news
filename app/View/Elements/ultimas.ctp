<style type="text/css">
    a.list-group-item {
        height:auto;
    }
    a.list-group-item.active{
        background-color: #428fff;
    }
    a.list-group-item.active small {
        color:#fff;
    }
    .stars {
        margin:20px auto 1px;    
    }
    .last{
        color: #000;
        background-color: #FFFFFF;
    }
    .last:active,.last:hover,.last:visited{
        text-decoration: none;
    }
    figure{width: 15% !important;
           margin: 2px 3px !important;
    }
    .panel-danger, .panel-default,.panel-primary{margin:2px 0 !important;}
    .panel-danger>.panel-heading{background: #009900;
    color: #FFFFFF;
    }
    .panel-danger{border-color: #009900;}
    
</style>

<div class="panel panel-danger">
    <div class="panel-heading">
        <h2 class="text-center">Últimas</h2>
    </div>
    <?php $news = $this->requestAction('news/listar/sort:created/direction:desc/limit:10'); ?>
    <?php foreach ($news as $new): ?>


        <?php if (isset($new['News']['archive'])) { ?>

                <figure class="pull-left">
                    <?php
                    echo $this->Html->image('/img/archive/' . $new['News']['id'] . '/thumb_' . $new['News']['archive'], array('class' => 'media-object img-responsive', 'alt' => $new['News']['archive']));
                    ?>
                </figure>
           
            <?php } ?>
            <spam class="pull-right"><?php echo $new['News']['created']; ?></spam>

            <h4 class="panel-title"> 
                <?php
                echo $this->Html->link($new['News']['title'], array('controller' => 'news',
                    'action' => 'visualizar', $new['News']['id']), array('class' => 'last'));
                ?>	
            </h4>
            <h5 class="panel-title">
                <small>
                    <?php echo $this->Html->link($new['News']['subtitle'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id']), array('class' => 'last'));
                    ?>
                </small>
            </h5>
        <hr>

    <?php endforeach; ?>
</div>
