<?php

App::uses('AppController', 'Controller');

class CategoriesController extends AppController {

    public $components = array('Paginator');

    public $paginate = array(
        'limit' => 10,
    );
    public function index() {
        $this->Category->recursive = 1;
        $this->Paginator->settings = $this->paginate;
        $cat = $this->Paginator->paginate();
        
        if ($this->request->is('requested')) {
            return $cat;
        } else {
            $this->set('categories', $cat);
        }
    }
    
    public function geral() {
        $this->Category->recursive = 1;
        $cat = $this->Paginator->paginate();
        
        if ($this->request->is('requested')) {
            return $cat;
        } else {
            $this->set('categories', $cat);
        }
    }

    public function view($id = null) {
        $this->layout = 'mobtek';
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
        $this->set('category', $this->Category->find('first', $options));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The category has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-error">The category could not be saved. Please, try again.</div>'));
            }
        }
        $sections = $this->Category->Section->find('list', array('fields' => 'section'));
        $this->set(compact('sections'));
        $news = $this->Category->News->find('list');
        $this->set(compact('news'));
    }

    public function edit($id = null) {
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('<div class="alert alert-success">The category has been saved.</div>'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-error">The category could not be saved. Please, try again.</div>'));
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $news = $this->Category->News->find('list');
        $this->set(compact('news'));
    }
    
    public function delete($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Category->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
