
--
-- Database: `portal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 10),
(4, NULL, 'Group', 2, NULL, 11, 20),
(5, 1, 'User', 1, NULL, 2, 3),
(6, 4, 'User', 2, NULL, 12, 13),
(7, 4, 'User', 4, NULL, 14, 15),
(8, 4, 'User', 5, NULL, 18, 19),
(9, NULL, 'User', 10, NULL, 21, 22),
(10, 4, 'User', 5, NULL, 16, 17),
(11, 1, 'User', 6, NULL, 4, 5),
(12, 1, 'User', 7, NULL, 6, 7),
(13, 1, 'User', 8, NULL, 8, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  `dir` varchar(45) DEFAULT NULL,
  `banner` varchar(80) NOT NULL,
  `created` date NOT NULL,
  `modified` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL,
  `section_id` int(11) NOT NULL,
  `color` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administradores', '2014-06-18 02:32:51', '2014-06-18 02:32:51'),
(2, 'Moderadores', '2014-06-18 02:35:44', '2014-06-18 02:35:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) NOT NULL,
  `dir` varchar(250) DEFAULT NULL,
  `resume` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `news_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `responsible_id` int(11) NOT NULL,
  `model_alias` varchar(50) NOT NULL,
  `model_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `log_detail_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `types` (`type`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `log_details`
--

CREATE TABLE IF NOT EXISTS `log_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `difference` text NOT NULL,
  `statement` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(45) NOT NULL,
  `archive` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `fonte` varchar(255) NOT NULL,
  `category_id` varchar(45) NOT NULL,
  `image_dir` varchar(45) DEFAULT NULL,
  `destaque` int(11) NOT NULL,
  `visualized` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `confirm` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `photo` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `photo_dir` varchar(45) DEFAULT NULL,
  `locked` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `confirm`, `group_id`, `created`, `modified`, `photo`, `email`, `photo_dir`, `locked`) VALUES
(1, 'admin', 'f73ebcdda3bfce90c2c034c7d518e21eec6adeb4', '', 1, '2014-06-18 02:40:07', '2014-07-25 20:07:42', 'oracle.jpg', 'wanderarce@yahoo.com.br', NULL, 0),
(2, 'editor', '06d0fb5a133894a22d7893e5514b2915f66630ff', '', 2, '2014-06-18 02:41:37', '2014-07-24 22:57:24', 'images.jpg', 'email@e.com', NULL, 0),
(3, 'wander', '06d0fb5a133894a22d7893e5514b2915f66630ff', '', 1, '2014-06-18 05:01:47', '2014-07-24 22:56:50', 'music.jpg', 'wander@teste.com', NULL, 0),
(4, 'teste', '65ab7214a614901765eeb53c4c6d02ee2045057f', '', 2, '2014-07-23 23:41:29', '2014-08-05 02:36:03', 'sql.jpg', 'teste@teste.com', '', 0),
(5, 'moderator', '784d885fb2dc573c9669e59c772579ba4802adf4', '784d885fb2dc573c9669e59c772579ba4802adf4', 2, '2015-01-17 12:19:30', '2015-01-17 14:28:42', '', 'moderator@news.com.br', NULL, 0),
(6, 'user', '43d84d549706fa4b3b511c5cdfa91f6e14c66ec4', '567890', 1, '2015-01-17 12:48:22', '2015-01-17 12:48:22', '', 'user@user.com', NULL, 0),
(7, 'Mobtek', '9a2213eb071a1977cabef59c11b63b5e8a3757a5', '9a2213eb071a1977cabef59c11b63b5e8a3757a5', 1, '2015-01-17 14:27:10', '2015-01-17 14:27:10', '', 'mobtek@mobtek.com.br', NULL, 0),
(8, 'tatyane', '0cf66c7d86907b81947e388c77250ef237ae5ff7', '0cf66c7d86907b81947e388c77250ef237ae5ff7', 1, '2015-01-17 14:30:01', '2015-01-17 14:30:01', '', 'tatyyylopess@hotmail.com', NULL, 0);
