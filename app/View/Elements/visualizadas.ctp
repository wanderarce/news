<style type="text/css">
    a.list-group-item {
        height:auto;
    }
    a.list-group-item.active{
        background-color: #428fff;
    }
    a.list-group-item.active small {
        color:#fff;
    }
    .stars {
        margin:20px auto 1px;    
    }
    .last{
        color: #000;
        background-color: #FFFFFF;
    }
    .panel-danger, .panel-default,.panel-primary{margin:3px 0 !important;}
    
    .panel-success>.panel-heading{background: #8A2BE2;
    color: #FFFFFF;}
    .panel-success{border-color: #8A2BE2;}
    
    figure{width: 15% !important;
           margin: 2px 3px !important;
    }
</style>



<div class="panel panel-success">
    <div class="panel-heading ">
        <h2 class="text-center">Mais Vistas</h2>
    </div>

    <?php $news = $this->requestAction('news/visualizadas/sort:created/direction:asc/limit:10'); ?>
    <?php foreach ($news as $new): ?>

        <?php if(isset($new['News']['archive'])) { ?>
            <figure class="pull-left">
                <?php
                echo $this->Html->image('/img/archive/'. $new['News']['id'] . '/thumb_' . $new['News']['archive'], array('class' => 'media-object img-rounded img-responsive', 'alt' => $new['News']['archive']));
                ?>
            </figure>

        <?php } ?>
        <b class="pull-right">
            <?php echo $new['News']['visualized']; ?>
            <small> views </small>
        </b>
        <h4 class="panel-title"><?php
            echo $this->Html->link($new['News']['title'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id']), array('class' => 'last'));
            ?>

        </h4>
        <h5 class="panel-title">
            <small><?php
                echo $this->Html->link($new['News']['subtitle'], array('controller' => 'news', 'action' => 'visualizar', $new['News']['id']), array('class' => 'last'));
                ?>
            </small>
        </h5>
        <hr>

    <?php endforeach; ?>
</div>
